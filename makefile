.PHONY: build
build: 
	docker image build --pull -t aspnetapp .

.PHONY: run
run:
	docker run --rm -it -p 9000:80 aspnetapp

.Phone: k8sbuild
k8sbuild:
	helm upgrade aspnet3release ./charts/aspnetapp/

.PHONY: k8s
k8s:
	kubectl port-forward service/aspnet3release-aspnetapp 9001:80
